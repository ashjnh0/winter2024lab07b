public class SimpleWar {
	
	public static void main(String[] args){
		
		System.out.println("Welcome to the card game Player 1!\n");
		Deck deck = new Deck();
		deck.shuffle();
		
		double player1Points = 0.0;
		double player2Points = 0.0;
		int round = 0;
		
		while(deck.length() > 2) {
			Card playerOne = deck.drawTopCard();
			Card playerTwo = deck.drawTopCard();
			round++;
			System.out.println("Round " + round + "\n");
			System.out.println("Player 1's card: " + playerOne);
			player1Points = playerOne.calculateScore();
			System.out.println("Player 1's points: " + player1Points);
			System.out.println("-------------------------------------------");
			System.out.println("Player 2's card: " + playerTwo);
			player2Points = playerTwo.calculateScore();
			System.out.println("Player 2's points: " + player2Points + "\n");
			System.out.println("-------------------------------------------");
			if (player1Points > player2Points) {
				System.out.println("Player 1 wins round " + round);
				System.out.println("-------------------------------------------");
			} else {
				System.out.println("Player 2 wins round " + round);
				System.out.println("-------------------------------------------");
			}
		}
		
		if (player1Points > player2Points) {
			System.out.println("Congratulations Player 1! You Win The Game!!");
		} else {
			System.out.println("You lose the game Player 1! Better luck next time!");
		}
	}
	
}
