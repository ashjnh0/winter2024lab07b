import java.util.Random;

public class Deck {

    private Card[] stack;
    private Random rng;
    private int length;

    public Deck() {
        rng = new Random();
        length = 52;
        Rank[] ranks = Rank.values();
        Suit[] suits = Suit.values();

        int i = 0;
        stack = new Card[length];

        for (Rank r : ranks) {
            for (Suit s : suits) {
                stack[i] = new Card(r, s); 
                i++;
            }
        }
    }

    public int length() {
        return this.length; 
    }

    public Card drawTopCard() {
        if(length > 0){
			Card topCard = this.stack[length - 1];
			length--;
			return topCard;
		} else {
			return null;
		}
    }

    public void shuffle() {
        for (int i = 0; i < length - 1; i++) {
            int randomIndex = rng.nextInt(length - i) + i;
            Card t = stack[i];
            stack[i] = stack[randomIndex];
            stack[randomIndex] = t;
        }
    }

    public String toString() {
        String s = "";
        for (Card c : this.stack) { 
            s += c.toString() + "\n";
        }
        return s;
    }
}
