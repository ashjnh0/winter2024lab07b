enum Rank {
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING 
}

enum Suit {
	CLUBS, DIAMONDS, HEARTS, SPADES
}
	
public class Card {
	
	private double score;
	private Rank rank;
	private Suit suit;
	
	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public Rank getRank() {
		return this.rank;
	}
	
	public Suit getSuit() {
		return this.suit;
	}
	
	public String toString() {
		return rank + " of " + suit;
	}
	
	public double calculateScore() {
		
		//ordinal() returns the position of each enum constant within the enum rank class and 
		//we add +1 to make it start from 1 and not 0 
		double score = rank.ordinal() + 1;
		
		switch(suit) {
			case HEARTS:
				score += 0.4;
				break;
			
			case SPADES:
				score += 0.3;
				break;
			
			case DIAMONDS:
				score += 0.2;
				break;
			
			case CLUBS:
				score += 0.1;
				break;
		}
		return score;
	}
}
